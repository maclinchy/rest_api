from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Stock
from .serializers import StockSerializer
from rest_framework.generics import UpdateAPIView

# List all stocks or create a new one
class StockList(APIView):

    def get(self, request):
        stocks = Stock.objects.all()
        serializer = StockSerializer(stocks, many=True)
        return Response(serializer.data)

    def post(self, request):
        newStock = StockSerializer(data=request.data)

        if newStock.is_valid():
            newStock.save()
        else:
            print(newStock.errors)

        return Response(newStock.data)

class StockEdit(UpdateAPIView):
    queryset = Stock.objects.all()
    serializer_class = StockSerializer
