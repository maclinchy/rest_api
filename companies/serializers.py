from rest_framework import serializers
from .models import Stock

class StockSerializer(serializers.ModelSerializer):

    class Meta:
        model = Stock
        fields = '__all__'

    def create(self, validated_data):
        stock = Stock.objects.create(**validated_data)
        return stock
